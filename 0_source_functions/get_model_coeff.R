# Function that extracts models coefficients, compares their MADs to Stuhler MADs, plots the coefficients and the 8 most important predictors----

# Needed package
library(tidyverse)

# Needed function to make code readable 
source("./0_source_functions/make_readable.R")

get_model_coeff <- function(model, outcome = c("relapse", "cdp")){
  
  # Summary of the posterior distribution of the model parameters including only fixed effects 
  model_coeff <- as.data.frame(summary(model, pars=c("beta", "alpha"), probs = c(0.025, 0.5, 0.975)))
  
  # Adding the standard error that is calculated based on the median absolute deviation (mad)
  #source("./0_source_functions/make_readable.R")
  model_coeff$MAD <- se(model)[1:55]
  Parameter <- rownames(model_coeff)
  model_coeff$Predictors <- make_readable(Parameter, data_full = data_full)
  
  # b. Comparing MAD to Stuhler et al.2020 MADs 
  # MAD reported by Stuhler et al.2020 extracted via a Java plot digitizer, accessible via:
  # http://plotdigitizer.sourceforge.net/
  stuhler_mad <- openxlsx::read.xlsx(xlsxFile = "./5_bayesian_model_development/Extracted data.xlsx", colNames = TRUE) %>% 
    select(1, "Stuhler_MAD_relapse" = 2,"Stuhler_MAD_cdp" = 3)
  
  coeff <- model_coeff %>%  
    select(Predictors, everything(), "OFSEP_MAD" = MAD) %>% 
    full_join(stuhler_mad)
  
  # calculating the spearman correlation coefficient between the MADs of Stuhler et al and our study. 
  if(outcome == "relapse"){
    rho_mad <- cor.test(coeff$OFSEP_MAD, coeff$Stuhler_MAD_relapse, method= "spearman")
  }
  
  if(outcome=="cdp"){
    rho_mad <- cor.test(coeff$OFSEP_MAD, coeff$Stuhler_MAD_cdp, method= "spearman")
  }
  
  # Rounding up numbers to 3 digits 
  coeff_round <- map_df(coeff[, 2:12], ~ round(., 3)) %>% 
    mutate(Predictor = model_coeff$Predictors, .before = 1)
  
  # Most important predictors
  imp_pred <- coeff_round %>% 
    dplyr::select(Predictor, Median = "50%", "MAD" = OFSEP_MAD) %>% 
    mutate(coeff = abs(Median)) %>% 
    arrange(desc(coeff)) %>% 
    slice_head(n = 8) %>% 
    rowid_to_column(var = "Rank") %>% 
    dplyr::select(- coeff)

  # Plotting median of fixed-effects coefficients and 95% credible interval
  MC_2 <- coeff_round %>%
    dplyr::select(Predictor, Median= "50%", cr_low= "2.5%", cr_high= "97.5%") %>% 
    ggplot(aes(x = Median, y = Predictor))+
    geom_vline(xintercept = 0, colour = "black", linetype = "dashed")+
    geom_point(aes(colour=Parameter), size = 0.8)+
    geom_errorbar(aes(xmin = cr_low, xmax = cr_high, colour = Parameter), linewidth = 0.8)+
    labs(x = "",  y = "Predictors", title = paste("Median and 95% credible interval fixed effects' posterior parameters of the", outcome, "model"))+
    theme_bw()+
    theme(plot.title = element_text(family = "Times", size = 15, face = "bold", hjust = 0),
          axis.text.y = element_text(size = 7.2), axis.ticks.y = element_blank(), legend.position = "None")

  # Used priors 
  priors <- prior_summary(model)
  
  ls <- list(rho_mad, coeff_round, imp_pred, MC_2, priors)
  names(ls) <- c("spear_corr_mad", "model coefficients", "important predictos", "model coefficients plot", "model priors") 
  return(ls)
}