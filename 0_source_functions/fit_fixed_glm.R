# Function to fit the fixed effects simple models to the data set split on 2017----

# Loading the needed packages----
my_packages <- c("tidyverse", "MASS")
lapply(my_packages, library, character.only = TRUE)

fit_fixed_glm <- function(training_data){
  # Relapse fixed effects model following a negative binomial distribution
  set.seed(01071989)
  relapse_model <- glm.nb(formula = n.relapse.xmt ~
                            f.xmt:z.n.relapse.prev.12m + 
                            f.xmt:f.flag.sl.prev.v2 +
                            f.xmt:f.gender +
                            f.xmt:z.t.onset.xmt.start.yrs +
                            f.xmt.prev:z.xmt.prev.duration.yrs +
                            f.tl.relapse.yrs + 
                            f.edss.bl.class.engler + 
                            f.flag.sl.prev.v2 + 
                            f.n.xmt.prev + 
                            z.n.relapse.prev.12m + 
                            z.t.onset.xmt.start.yrs +
                            f.gender + 
                            f.age.xmt +
                            z.xmt.prev.duration.yrs + 
                            f.xmt.prev +
                            f.xmt +
                            offset(log(xmt.duration.yrs)), 
                          data = training_data)
  
  # CDP fixed effects model following a binomial distribution
  set.seed(01071989)
  cdp_model <- glm(formula = cdp ~
                     f.xmt:z.n.relapse.prev.12m + 
                     f.xmt:f.flag.sl.prev.v2 +
                     f.xmt:f.gender +
                     f.xmt:z.t.onset.xmt.start.yrs +
                     f.xmt.prev:z.xmt.prev.duration.yrs +
                     f.tl.relapse.yrs + 
                     f.edss.bl.class.engler + 
                     f.flag.sl.prev.v2 + 
                     f.n.xmt.prev + 
                     z.n.relapse.prev.12m + 
                     z.t.onset.xmt.start.yrs +
                     f.gender + 
                     f.age.xmt +
                     z.xmt.prev.duration.yrs + 
                     f.xmt.prev +
                     f.xmt,
                   data = training_data,
                   family = binomial, 
                   offset = log(xmt.duration.yrs))  
  
  return(list(relapse_model = relapse_model, cdp_model = cdp_model))
}