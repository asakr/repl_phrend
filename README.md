---
bibliography: references.bib
---

![]()

Code to reproduce the results of the study:

***Framework for Personalized Prediction of Treatment Response in Relapsing-Remitting Multiple Sclerosis: A Replication Study in Independent Data.***

Authors: Anna Maria Sakr1, Ulrich Mansmann1, Joachim Havla2, Begum Irmak Ön1

Affiliations:

1.  Institute for Medical Information Processing, Biometry and Epidemiology - IBE, LMU Munich, Marchioninistrasse 15, 81377 Munich, Germany

2.  Institute of Clinical Neuroimmunology, LMU Hospital, Marchioninistrasse 15, 81377 Munich, Germany

Corresponding author: Ulrich Mansmann

**DESCRIPTION**

This study is conducted using the French MS registry OFSEP data to replicate and externally validate prediction models developed by @Stühler2020 using the German MS registry NeuroTransData (NTD).

To reproduce our study's results, access to the data subset used in this study needs to be granted from OFSEP.

Then, our published code should be run in the below order:

1_quality_check.R

-   In this script we applied quality criteria to the raw OFSEP subset, based on @Stühler2020, Additional file 1, Table S1.A. (<https://bmcmedresmethodol.biomedcentral.com/articles/10.1186/s12874-020-0906-6>).

2.1_derived_dataset.R

-   In this script we derived the analysis data set and parametrized the variables based on the predictors list of @Stühler2020 (table 1) and their CDP outcome definition (Supplementary file 2).

2.2_missing_data.R

-   In this script we checked the missingness in the raw and derived data sets.

3.1_filtration_criteria.R

-   In this script we applied filtration criteria to the derived data set, based on @Stühler2020, Additional file 1, Table S1.A.

3.2_temporal_split.R

-   In this script we temporally split the filtered data into training and test sets

4_patient_characteristics.R

-   In this script we described and summarized the predictors and clinical outcomes.

5_bayesian_model_development.R

-   In this script we built and fit bayesian hierarchical models to the training set, similarly to @Stühler2020 and checked their diagnostics.

6.1_bayesian_perf_test_set.R

-   In this script we checked the discriminative performance, calibration and global fit of the Bayesian models using the test set.

6.2_cross_validation.R

-   In this script we did a 10-fold cross-validation using the training set, assessing the same performance measures as for the test set validation.

7_fixed_effect_glms.R

-   In this script we fit fixed effects simple generalized linear models to the training data (2017 split), assessed their performance and compared their coefficients to the Bayesian models.

Note: For all scripts, the user will need to change the path to the needed input data and the generated output data.

**References**
